from events.keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY
import requests
import json


def get_city_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city},{state}"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    photo = json.loads(response.content)
    return {"photo_url": photo["photos"][0]["src"]["original"]}


def get_weather(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&appid="
    response = requests.get(url + OPEN_WEATHER_API_KEY)
    geolocation = json.loads(response.content)
    lon = geolocation[0]["lon"]
    lat = geolocation[0]["lat"]

    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid="
    response = requests.get(url + OPEN_WEATHER_API_KEY)
    weather = json.loads(response.content)
    temp = weather["main"]["temp"]
    description = weather["weather"][0]["description"]

    return {
        "temp": temp,
        "description": description
        }
